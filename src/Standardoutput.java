import java.io.PrintStream;

public class Standardoutput {
    public static void main(String[] args){
        PrintStream system = new PrintStream(System.out);
        system.println("\"" + "true");
        system.println('A');
        system.println("ABC");
        system.println(3.5);
        system.println(-9.3);
        system.println(88);
        system.println(9000000);
        system.println("abc" + "\"");

        system.flush();
        system.close();
}
}
